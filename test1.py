#_*_coding:utf-8_*_
# Date:.2019/1/12

import requests
import re
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt

ret = requests.get("http://zypt.neusoft.edu.cn/hasdb/pubfiles/gongshi2016/detail/10460/10460_080901/10460_080901_TK_XSJYQX.html")
ret.encoding = "utf-8"
soup = BeautifulSoup(ret.text,"html.parser")
table_img = soup.find(name="table", attrs={"id":"collegeDataTable"})
th_img = table_img.find_all(name="th")
td_img = table_img.find_all(name="td")

labels_list = []
for i in th_img:
    labels_list.append(i.text)
new_labels_list = labels_list[2:-1]
# print(labels_list)

data_list = []
for j in td_img:
    data_list.append(j.text)

for k in range(4):
    year = data_list[0]
    total_people = int(data_list[1])
    data_list1 = data_list[:len(labels_list)]
    new_data_list = data_list1[2:-1]
    del data_list[:len(labels_list)]
    labels = [u'政府机构', u'事业单位', u'企业', u'部队', u'灵活就业', u'出国', u'升学', u'参加国家地方项目就业', u'其他']
    sizes = [int(m)/total_people for m in new_data_list]
    colors = 'yellow', 'gold', 'lightskyblue', 'lightcoral','chartreuse','blue','darkorange','green','purple'
    explode = [0, 0.1, 0, 0, 0, 0, 0, 0, 0, 0]     #0.1表示将那一块凸显出来
    plt.pie(sizes, labels=labels, colors=colors, autopct='%1.1f%%', shadow=True, startangle=50)
    plt.rcParams['font.sans-serif'] = ['simhei']
    plt.rcParams['axes.unicode_minus'] = False
    plt.axis('equal')
    plt.show()