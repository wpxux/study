import requests
import re
from bs4 import BeautifulSoup
# from app01 import models

# 获取参评学校的相关信息
def get_img(url):
    res = requests.get(url)
    res.encoding = "utf-8"
    soup1 = BeautifulSoup(res.text, "html.parser")
    tag = soup1.find(name="tbody")
    all_td = tag.find_all(name="td")
    url_ = url+"college/%s.html"
    for i in all_td:
        college_img = i.text
        college_id = re.match('\d+', college_img).group()
        col_count = re.search('\\(\d\\)', college_img).group()
        new_url = url_%college_id
        # 拼接成每一个查看详细信息的url
        a = i.find(name='a')
        print(college_id, a.text, col_count, new_url)
        # models.Img.objects.create(code=college_id, college=a.text, count=col_count, url=new_url)

url = "http://zypt.neusoft.edu.cn/hasdb/pubfiles/gongshi2016/"
ret = get_img(url)

# 获取每个学校参评的所有专业
ret1 = requests.get('http://zypt.neusoft.edu.cn/hasdb/pubfiles/gongshi2016/college/10459.html')
ret1.encoding = "utf-8"
soup = BeautifulSoup(ret.text, "html.parser")
tab_img = soup.find(name="tbody")
a_img = tab_img.find_all(name="a")
ls = []
for i in range(len(a_img)):
    # print(a_img[i].text)
    if i%2 == 0:
        ls.append(a_img[i].text)
    else:
        continue

print(ls)