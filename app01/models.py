from django.db import models

# Create your models here.


class Img(models.Model):
    """
    学校信息表
    """
    nid = models.AutoField(primary_key=True)
    code = models.CharField(max_length=10, verbose_name="学校代码")
    college = models.CharField(max_length=32, verbose_name="学校名称")
    count = models.CharField(max_length=4, verbose_name="参评专业数量")
    url = models.CharField(max_length=128, verbose_name="查看详细信息")


