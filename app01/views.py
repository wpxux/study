from django.shortcuts import render
import requests
import re
from bs4 import BeautifulSoup
from app01 import models
from wordcloud import WordCloud,ImageColorGenerator
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np


def index(request):
    # res = requests.get("http://zypt.neusoft.edu.cn/hasdb/pubfiles/gongshi2016/")
    # res.encoding = "utf-8"
    # soup1 = BeautifulSoup(res.text, "html.parser")
    # tag = soup1.find(name="tbody")
    # all_td = tag.find_all(name="td")
    # url = "http://zypt.neusoft.edu.cn/hasdb/pubfiles/gongshi2016/college/%s.html"
    # for i in all_td:
    #     college_img = i.text
    #     college_id = re.match('\d+', college_img).group()
    #     col_count = re.search('\\(\d\\)', college_img).group()
    #     new_url = url%college_id
    #     # 拼接成每一个查看详细信息的url
    #     a = i.find(name='a')
    #     # print(college_id, a.text, col_count, new_url)
    #     models.Img.objects.create(code=college_id, college=a.text, count=col_count, url=new_url)

    # 获取参评最多专业的学习
    count_list = models.Img.objects.values("count")
    # 参与的学校总数
    all_count = len(count_list)

    ls2 = []
    for i in count_list:
        ls2.append(int(i["count"][1]))
    # 找出参评专业数最高的数量
    max_count = max(ls2)
    # 从数据库中找出专业数最高的学校的名称
    max_college = models.Img.objects.get(count="(%s)"%max_count)
    # print(max_college.college)

    page_num = request.GET.get("page")
    total_count = models.Img.objects.count()
    from utils.mypage import Page
    page_obj = Page(page_num, total_count, url_prefix="/index/", per_page=10, max_page=9, )
    page_html = page_obj.page_html()
    ret = models.Img.objects.all()[page_obj.start:page_obj.end]
    return render(request, "index.html",
                  {"ret_list": ret,
                   "page_html": page_html,
                   "max_count":max_count, "max_college":max_college,
                   "all_count":all_count,
                   })


def detail(request):
    url_list = models.Img.objects.all().values("url")
    # 从数据库中获取所有的查看详细信息的url
    ls = []
    for i in url_list:
        ret = requests.get(i["url"])
        ret.encoding = "utf-8"
        soup = BeautifulSoup(ret.text, "html.parser")
        tab_img = soup.find(name="tbody")
        a_img = tab_img.find_all(name="a")
        for i in range(len(a_img)):
            # print(a_img[i].text)
            if i % 2 == 0:
                ls.append(a_img[i].text)
            else:
                continue

    text = " ".join(ls)
    text = text*3
    # 生成一个词云图像
    wordcloud = WordCloud(max_font_size=66,
                          font_path="c:/Windows/Fonts/simfang.ttf",
                          background_color="black",
                          width=500, height=280
                          ).generate(text)
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    aim_path = 'static/img/zhuanye.png'
    plt.savefig(aim_path)
    plt.show()

    image = Image.open(r'E:\python之路\pydesign\static\img\reqiqiu.png')
    img = np.array(image)
    wc = WordCloud(font_path="C:\Windows\Fonts\STHUPO.ttf", background_color='white',mask=img)
    wc.generate(text)
    # 绘制文字的颜色以背景图颜色为参考
    image_color = ImageColorGenerator(img)
    wc.recolor(color_func=image_color)
    # 图片的展示
    plt.imshow(wc)
    plt.axis("off")
    plt.savefig('static/img/ciyun.png')

    return render(request, "detail.html")


def infoall(request):
    # 爬取专业评估申报的信息
    url = 'http://zypt.neusoft.edu.cn/hasdb/pubfiles/gongshi2016/detail/10463/10463_080901.html'
    res = requests.get(url)
    res.encoding = "utf-8"
    soup2 = BeautifulSoup(res.text, "html.parser")
    tab_img = soup2.find(name="tbody")
    td_img = tab_img.find_all(name="tr", attrs={"class": "subtitle"})
    content_list = []
    for i in td_img:
        content = re.search(">\d.+<", str(i))
        if content:
            content_list.append(content.group()[1:-1])

    # 爬取专业评估申报的所需的详细信息
    a_tag = tab_img.find_all(name="a")

    xh = tab_img.find_all(attrs={"class": "subitem"})
    xh_list = []
    for j in range(len(xh)):
        xh_list.append(xh[j].text)

    detail_list = []
    for k in range(len(a_tag)):
        if k % 2 == 0:
            detail_list.append(a_tag[k].text)
        else:
            continue

    ret_list = []
    for i in range(len(detail_list)):
        ret_list.append("{} {}".format(xh_list[i], detail_list[i]))

    page_num = request.GET.get("page")
    total_count = len(detail_list)
    from utils.mypage import Page
    page_obj = Page(page_num, total_count, url_prefix="/infoall/", per_page=10, max_page=9)
    page_html = page_obj.page_html()
    ret_list = ret_list[page_obj.start:page_obj.end]


    return render(request,"info.html",{"content_list":content_list, "ret_list":ret_list,
                                       "page_html": page_html})